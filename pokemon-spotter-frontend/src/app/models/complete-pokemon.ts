export interface CompletePokemon {
    id: number;
    name: string;
    spriteUrl: string;
    types: string[];
}