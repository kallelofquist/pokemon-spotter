export interface ShortPokemon {
    name: string;
    url: string;
    spriteUrl: string;
}