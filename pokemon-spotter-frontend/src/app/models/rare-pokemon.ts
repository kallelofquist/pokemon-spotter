export interface RarePokemon {
    timesbeenspotted: number;
    pokeid: number;
    name: string;
    spriteUrl: string;
}