import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterUserComponent } from './components/register-user/register-user.component';
import { LoginUserComponent } from './components/login-user/login-user.component';
import { DataComponent } from './components/data/data.component';
import { ListAllPokemonComponent } from './components/list-all-pokemon/list-all-pokemon.component';
import { RegisterPokemonComponent } from './components/register-pokemon/register-pokemon.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AnonymousIndexComponent } from './components/anonymous-index/anonymous-index.component';



const routes: Routes = [
  {
    path: 'listAll',
    component: ListAllPokemonComponent
  },
  { 
    path: 'registerPokemon/:name',
    component: RegisterPokemonComponent 
  },
  {
    path: 'register',
    component: RegisterUserComponent
  },
  {
    path: 'login',
    component: LoginUserComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  }, 
  {
    path: 'data',
    component: DataComponent
  },
  {
    path: 'anonymousIndex',
    component: AnonymousIndexComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/anonymousIndex'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }