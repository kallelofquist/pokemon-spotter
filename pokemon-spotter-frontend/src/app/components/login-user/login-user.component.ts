import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FormControl, ReactiveFormsModule } from '@angular/forms'
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'


@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.scss']
})
export class LoginUserComponent implements OnInit {
  username
  password
  currentSession: boolean = false;
  isLoading: boolean = false;
  verify: boolean = null;
  bar1: boolean = null;
  bar2: boolean = null;
  bar3: boolean = null;

  constructor(private session: SessionService, private router: Router, private auth: AuthService, private http: HttpClient) {

    if (this.session.get() !== false) {
      this.currentSession = true;
      console.log("Existing user: " + localStorage.getItem('sp_session_username'))
      window.location.href = ("/profile")
    }
  }

  ngOnInit(): void {
  }

  verifyCorrect(currentUser) {
    console.log("Welcome " + currentUser[0].username)
    console.log("email:" + currentUser[0].email)
    localStorage.setItem('sp_session_username', currentUser[0].username)
    localStorage.setItem('sp_session_email', currentUser[0].email)

    //Small display feature, with moving progress-bar during login. 
    this.progressBar()
  }
  verifyDenied() {
    console.log("Access denied")
    this.verify = false; 
  }

  login(userin, pwin): Promise<any> {

    return this.http.post(`${environment.apiUrl}/verifyuser`, {
      username: userin,
      password: pwin
    }).toPromise()
      .then(res => {
        console.log(res)
        if (res[0] === undefined) {
          this.verifyDenied();
        }
        else if (res[0].username === userin) {
          this.verifyCorrect(res);
        }         

        return res
      })

  }

  progressBar(){
    this.verify = true;
    this.bar1 = true;

    setTimeout(() => {
      this.bar1 = false;;
      this.bar2 = true;
    }, 400)
    setTimeout(() => {
      this.bar2 = false;
      this.bar3 = true;
    }, 500)
    setTimeout(() => {
      this.router.navigate(['/profile']);
      window.location.reload();
    }, 1500)
  }


}
