import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnonymousIndexComponent } from './anonymous-index.component';

describe('AnonymousIndexComponent', () => {
  let component: AnonymousIndexComponent;
  let fixture: ComponentFixture<AnonymousIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnonymousIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnonymousIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
