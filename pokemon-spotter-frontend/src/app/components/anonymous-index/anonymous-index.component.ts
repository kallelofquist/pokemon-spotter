import { Component, OnInit } from '@angular/core';

import { PokeCallerService } from '../../services/poke-service/poke-caller.service';
import { TopUser } from '../../models/top-user';
import { RarePokemon } from '../../models/rare-pokemon';

@Component({
  selector: 'app-anonymous-index',
  templateUrl: './anonymous-index.component.html',
  styleUrls: ['./anonymous-index.component.scss']
})
export class AnonymousIndexComponent implements OnInit {

  constructor(private pokeCallerService: PokeCallerService) { }

  userArray: TopUser[] = [];
  rareArray: RarePokemon[] = [];

  ngOnInit(): void {
    this.getTopTenUsers();
    this.getTenRarest();
  }

  getTopTenUsers(): void {
    this.pokeCallerService.getTopTenUsers().subscribe(response => {
      this.userArray = response;
    });
  }

  getTenRarest(): void {
    this.pokeCallerService.getTenRarest().subscribe(response => {
      this.rareArray = response;
      console.log(this.rareArray)
      this.getCompletePokeInfo();
    });
  }

  getCompletePokeInfo() {
    for(let i = 0; i < this.rareArray.length; i++) {
      this.pokeCallerService.getSpecificPokemon(null, this.rareArray[i].pokeid).subscribe(response => {
        this.rareArray[i].pokeid = response.id;
        this.rareArray[i].name = response.species.name;
        this.rareArray[i].spriteUrl = response.sprites.front_default;
      });
    }
  }

}
