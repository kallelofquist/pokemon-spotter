import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'
import { CompletePokemon } from 'src/app/models/complete-pokemon';
import { PokeCallerService } from 'src/app/services/poke-service/poke-caller.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  user = localStorage.getItem('sp_session_username')


  constructor(private http: HttpClient, private poke: PokeCallerService) {

  }

  pokemonArray = [
    {
      username: '',
      spotted: [
        {
          pokeid: null,
          shiny: null,
          gender: null,
          longitude: null,
          latitude: null
        },
      ]
    }
  ];

  completePokemons = []

 

  ngOnInit(): void {
        this.getFullInfo();

  }

  async getFullInfo() {
    await this.getPokemons(this.user);

    //console.log(this.pokemonArray[0].spotted[0].shiny)

    for (let i = 0; i < this.pokemonArray[0].spotted.length; i++) {
      console.log(this.pokemonArray[0].spotted[i].pokeid)
      this.poke.getSpecificPokemon(null, this.pokemonArray[0].spotted[i].pokeid)
      .subscribe(
        (val) => this.addPoke(val, this.pokemonArray[0].spotted[i].shiny, this.pokemonArray[0].spotted[i].gender,
           this.pokemonArray[0].spotted[i].longitude, this.pokemonArray[0].spotted[i].latitude) //Include gender, shiny etc. 
        )
        
    }
  }
  addPoke(pokemon, shiny, gender, longitude, latitude){
    //let pokemon = this.poke.getSpecificPokemon(pokeId);
    let pokeObject = {
    name: '',
    url: '',
    gender: '',
    longitude: '',
    latitude: ''
  }
    pokeObject.name = pokemon.species.name
    if(shiny){
          pokeObject.url = pokemon.sprites.front_shiny

    }else{
      pokeObject.url = pokemon.sprites.front_default
    }
    if(gender) {
      pokeObject.gender = 'Male'
    } else{
      pokeObject.gender = 'Female'
    }
    pokeObject.longitude = longitude;
    pokeObject.latitude = latitude;
    this.completePokemons.push(pokeObject)
    //this.completePokemons.push(pokemon)
    console.log(this.completePokemons)
  }
  getPokemons(userin): Promise<any> {

    return this.http.post(`${environment.apiUrl}/getuserinfo`, {
      username: userin,
    }).toPromise()
      .then((res: any) => {

        for (let i = 0; i < res.spotted.length; i++) {
          this.pokemonArray[0].spotted[i] = res.spotted[i]
        }
        return this.pokemonArray
      })
  }



  logoutUser() {
    //clear local storage on LogOut
    localStorage.clear();
    window.location.href = "/login"
  }

}
