import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { PokeCallerService } from '../../services/poke-service/poke-caller.service';
import { CompletePokemon } from '../../models/complete-pokemon';
import { SessionService } from '../../services/session/session.service';

@Component({
  selector: 'app-register-pokemon',
  templateUrl: './register-pokemon.component.html',
  styleUrls: ['./register-pokemon.component.scss']
})

export class RegisterPokemonComponent implements OnInit {

  sub: Subscription;

  //Get the current user through the current session
  spottedby: string = this.session.get();

  latitude: number = 0;
  longitude: number = 0;

  gender: string = 'm';
  shiny: string = 'n';

  completePokemon: CompletePokemon = {
    id: null,
    name: null,
    spriteUrl: null,
    types: []
  };

  constructor(private route: ActivatedRoute, private pokeCallerService: PokeCallerService, private session: SessionService) {
   }

  ngOnInit() {
    
    this.sub = this.route.params.subscribe(params => {
       
      this.completePokemon.name = params['name']; 
       
       this.pokeCallerService.getSpecificPokemon(this.completePokemon.name).subscribe(response => {
        
        this.completePokemon.id = response.id;
        this.completePokemon.spriteUrl = response.sprites.front_default;
        
        response.types.forEach(element => {

          this.completePokemon.types.push(element.type.name);

        });

      });

    });

  }

  sendForm(): void {

    let jsonObject: object = {
      spottedby: this.spottedby,
      pokeid: this.completePokemon.id,
      shiny: this.gender == 'y' ? true : false,
      gender: this.gender == 'm' ? true : false,
      longitude: this.longitude,
      latitude: this.latitude
    };

    let body: string = JSON.stringify(jsonObject);

    this.pokeCallerService.postSpotting(body);

    console.log(body);


    //window.location.href = ("/profile")

  }

}
