import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SessionService } from 'src/app/services/session/session.service'
import { FormControl, ReactiveFormsModule } from '@angular/forms'
import { RegisterUserComponent } from '../register-user/register-user.component';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent implements OnInit {

  user = {
    username: '',
    mail: '',
    pw: ''
  };

  logoutUser(){
    //clear local storage on LogOut
    localStorage.clear();
  }

  constructor(private auth: AuthService, private session: SessionService) {
    //Call the register-method
    this.register()

  }
  ngOnInit(): void {
  }

  async register() {
    try {
      //Call the function "adding()" within the imported class "AuthService". Store the data in result 

      const result: any = await this.auth.adding();

      //Print current user's data, see what is possible to extract!
      console.log(result)

      //If there is data, store the needed data in the locally declared user object
      if (result.username != '') {
        this.user.username = result.username;
        this.user.mail = result.email;
        this.user.pw = localStorage.getItem('sp_session_password')

        this.session.save({
          username: result.username,
          mail: result.email
        })

        //Also post to the database!
        await this.auth.addingDB(this.user);
      }

      window.location.href = ("/profile")
    }
    catch (e) {
      console.log("Error boiii")
    }
    finally {
    }
  }

}
