import { Component, OnInit } from '@angular/core';

import { ShortPokemon } from '../../models/short-pokemon';
import { PokeCallerService } from '../../services/poke-service/poke-caller.service'

@Component({
  selector: 'app-list-all-pokemon',
  templateUrl: './list-all-pokemon.component.html',
  styleUrls: ['./list-all-pokemon.component.scss']
})
export class ListAllPokemonComponent implements OnInit {

  pokemonArray: ShortPokemon[] = [];

  //Store the current user in Storage
  user = localStorage.getItem('sp_session_username')

  constructor(private pokeCallerService: PokeCallerService) {}

  ngOnInit() {
    this.getPokemon();
  }

  getPokemon(): void {

    this.pokeCallerService.getPokemon().subscribe(response => {
      this.pokemonArray = response.results;

      for (let i = 0; i < this.pokemonArray.length; i++) {

        let pokemonIndex = i + 1;

        this.pokemonArray[i].spriteUrl = this.pokeCallerService.getSprite(pokemonIndex);
        
      }

    });

  }

  logoutUser(){
    //clear local storage on LogOut
    localStorage.clear();
    window.location.href = "/login"
  }

}
