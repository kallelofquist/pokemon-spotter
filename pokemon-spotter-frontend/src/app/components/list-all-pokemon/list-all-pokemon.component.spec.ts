import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAllPokemonComponent } from './list-all-pokemon.component';

describe('ListAllPokemonComponent', () => {
  let component: ListAllPokemonComponent;
  let fixture: ComponentFixture<ListAllPokemonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAllPokemonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAllPokemonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
