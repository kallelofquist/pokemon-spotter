import { Component, OnInit } from '@angular/core';

import { FormControl, ReactiveFormsModule } from '@angular/forms'
import { SessionService } from 'src/app/services/session/session.service';

const auth_config = {
  redirect_uri: 'http://localhost:4200/data', // change this for your app
  client_id: '7b2eef62bd2e3e9d9437afa1bc17dacf3015d31cb24005311acda175f7641b3f',
  scope: 'read_user openid profile email', // This should be enough
  response_type: 'token',
}

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})

export class RegisterUserComponent implements OnInit {

  
  password = new FormControl('');

  constructor(private session: SessionService) {

    if (this.session.get() !== false) {
      console.log("Already a user here, name: " + localStorage.getItem('sp_session_username'))
      window.location.href = "/profile"
    }
  }

  ngOnInit(): void {
  }

  doAuth() {
    localStorage.setItem('sp_session_password', this.password.value)
    window.location.href = "https://gitlab.com/oauth/authorize?" + `${new URLSearchParams(auth_config).toString()}`
  }
}


