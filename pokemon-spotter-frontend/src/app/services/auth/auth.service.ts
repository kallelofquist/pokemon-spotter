import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
    }

  adding(): Promise<any> {
    //Displays the data from a connected user. 
    console.log(window.location.href)

    if (window.location.hash) {

      let fragment_access_token = (new URLSearchParams(location.hash.substring(1)).get("access_token"))

      //const headers  = { 'Authorization': `Bearer ${fragment_access_token}` }
      return this.http.get('https://gitlab.com/api/v4/user', {
        headers: {
          Authorization: `Bearer ${fragment_access_token}`,
        },
      })
        .toPromise()
        .catch(res => res.json())
        .then(data => {
          document.getElementById("json").textContent = JSON.stringify(data, undefined, 2)

          return data
        })
    }
  }

  addingDB(user): Promise<any> {

    return this.http.post(`${environment.apiUrl}/createuser`, {
     username: user.username,
     password: user.pw
    }).toPromise();
  }
}
