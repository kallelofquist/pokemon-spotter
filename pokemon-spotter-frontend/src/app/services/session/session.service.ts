import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  //Save the new user to memory, rather username and mail seperated, than a string of such characters: {":"}
  save(session: any) {
    //localStorage.setItem('sp_session', JSON.stringify(session))
    localStorage.setItem('sp_session_username', session.username)
    localStorage.setItem('sp_session_mail', session.mail)

  }

  //To check if there is an existing user, get the sp_session in local storage
  get(): any {
    const savedSession = localStorage.getItem('sp_session_username');

    return savedSession ? savedSession : false
  }
}
