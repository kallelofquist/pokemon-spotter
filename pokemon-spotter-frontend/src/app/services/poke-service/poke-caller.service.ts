import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class PokeCallerService {

  constructor(private http: HttpClient) { }

  apiEnvironment = environment;

  private pokeapiUrl = 'https://pokeapi.co/api/v2/pokemon?limit=151';  // URL to web api
  private spriteUrl = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'; // URL to pokémon sprite - needs extension ex. '1.png'
  private pokemonUrl = 'https://pokeapi.co/api/v2/pokemon/';  // URL to specific pokémon information - needs extension ex. 'bulbasaur' or '1'

  getPokemon(): Observable<any> {
    return this.http.get<any>(this.pokeapiUrl);
  }

  getSprite(pokemonNumber: number): string {
    return this.spriteUrl + pokemonNumber.toString() + '.png';
  }

  getSpecificPokemon(pokemonName?: string, pokemonId?: number): Observable<any> {

    let completeUrl: string;

    if(pokemonName) {
      completeUrl = this.pokemonUrl + pokemonName;
    }
    else if(pokemonId) {
      completeUrl = this.pokemonUrl + pokemonId;
    }
    else {
      console.log("No pokémon name OR id!");
      return null;
    }
    return this.http.get<any>(completeUrl);
  }

  getTopTenUsers(): Observable<any> {
    return this.http.get<any>(`${this.apiEnvironment.apiUrl}/topspotters`);
  }

  getTenRarest(): Observable<any> {
    return this.http.get<any>(`${this.apiEnvironment.apiUrl}/getrarest`);
  }

  postSpotting(jsonObject: string): Promise<any> {
    return this.http.post(`${this.apiEnvironment.apiUrl}/addencounter`, JSON.parse(jsonObject)).toPromise();
  }
}
