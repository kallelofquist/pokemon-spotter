import { TestBed } from '@angular/core/testing';

import { PokeCallerService } from './poke-caller.service';

describe('PokeCallerService', () => {
  let service: PokeCallerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PokeCallerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
