import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms'

//Components
import { AppComponent } from './app.component';
import { ListAllPokemonComponent } from './components/list-all-pokemon/list-all-pokemon.component';
import { RegisterPokemonComponent } from './components/register-pokemon/register-pokemon.component';
import { LoginUserComponent } from './components/login-user/login-user.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { DataComponent } from './components/data/data.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AnonymousIndexComponent } from './components/anonymous-index/anonymous-index.component';

@NgModule({
  declarations: [
    AppComponent,
    ListAllPokemonComponent,
    RegisterPokemonComponent,
    LoginUserComponent,
    RegisterUserComponent,
    DataComponent,
    ProfileComponent,
    AnonymousIndexComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
